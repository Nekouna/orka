#!/usr/bin/env python

import gitlab
import os
import sys
from inventory import OrkaManager

PROJECT_ID = "gitlab-org/ci-cd/shared-runners/images/macstadium/orka"

orka_api_token = os.getenv("ORKA_API_TOKEN", "")
orka_api_url = os.getenv("ORKA_API_URL", "")
gitlab_token = os.getenv("GITLAB_TOKEN", "")

if orka_api_token == "" or orka_api_url == "" or gitlab_token == "":
  print("Please define the env vars $ORKA_API_TOKEN, $ORKA_API_URL and $GITLAB_TOKEN")
  sys.exit(1)

gl = gitlab.Gitlab('https://www.gitlab.com', private_token=gitlab_token)
orka = OrkaManager(orka_api_token, orka_api_url)

print("Getting gitlab project %s" % PROJECT_ID)
project = gl.projects.get(PROJECT_ID)

print("Getting running VMs...")
vms = [vm["virtual_machine_name"] for vm in orka.list_vms()]

print("Getting running jobs...")
jobs = [job.id for job in project.jobs.list(scope="running")]
print("Running jobs are: %s" % jobs)

# only clean vms associated with orka repo jobs
vms = [vm for vm in vms if vm.startswith('runners-macos-ci-')]

# don't clean vms associated with running jobs
for job in jobs:
  vms = [vm for vm in vms if not vm.startswith('runners-macos-ci-%d' % job)]

print("VMs to delete: %s" % vms)

for vm in vms:
  resp = orka.purge_vm(vm)
  print(resp.get('message', resp))

print("Getting open MRs...")
mrs = project.mergerequests.list(state='opened')
mr_iids = [mr.iid for mr in mrs]
print("Open MRs are %s" % mr_iids)

print("Getting all images...")
images = [image["image"] for image in orka.images()]

# only clean images associated with MRs
images = [image for image in images if image.startswith('mr-')]

# don't clean images associated with an open MR
for mr_iid in mr_iids:
  images = [image for image in images if not image.startswith('mr-%s' % mr_iid)]

print("Images to delete: %s" % images)

for image in images:
  resp = orka.delete_image(image)
  print(resp.get('message', resp))

print("Done")
