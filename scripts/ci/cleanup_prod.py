#!/usr/bin/env python

import os
import sys
from datetime import datetime, timezone
from inventory import OrkaManager

orka_api_token_canary = os.getenv("ORKA_API_TOKEN_CANARY", "")
orka_api_token_beta = os.getenv("ORKA_API_TOKEN_BETA", "")
orka_api_url = os.getenv("ORKA_API_URL", "")

if orka_api_token_canary == "" or orka_api_token_beta == "" or orka_api_url == "" :
  print("Please define the env vars $ORKA_API_TOKEN_CANARY, $ORKA_API_TOKEN_BETA, $ORKA_API_URL")
  sys.exit(1)

def delete_old_vms(token, env):
  print("Checking old %s VMs..." % env)

  orka = OrkaManager(token, orka_api_url)
  vms = [vm for config in orka.list_vms() for vm in config.get("status", [])]
  now = datetime.now(timezone.utc)
  to_delete = []

  for vm in vms:
    creation_timestamp = datetime.strptime(vm['creation_timestamp'], '%Y-%m-%dT%H:%M:%S%z')
    delta = now - creation_timestamp
    if delta.total_seconds() > 4*60*60: # delete VMs older than 4 hours
      to_delete.append(vm['virtual_machine_id'])

  print("VMs to delete: %s" % to_delete)
  for vm in to_delete:
      resp = orka.delete_vm(vm)
      print(resp.get('message', resp))

delete_old_vms(orka_api_token_canary, "canary")
delete_old_vms(orka_api_token_beta, "beta")
