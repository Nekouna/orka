#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

if ! command -v op &> /dev/null; then echo '1password-cli not found. Install with "brew install 1password-cli "'; exit 1; fi
if ! command -v openconnect &> /dev/null; then echo 'openconnect not found. Install with "brew install openconnect "'; exit 1; fi

echo "Logging in to 1password"
eval "$(op signin gitlab.1password.com)"

ENVIRONMENT=${1:-prod}
ONEP_ENTRY="Orka VPN"
if [ "$ENVIRONMENT" != "prod" ]; then
  ONEP_ENTRY="Orka VPN Dev"
fi

ORKA_VPN_USER="$(op get item --vault Verify --account gitlab --fields username "$ONEP_ENTRY")"
ORKA_VPN_PASS="$(op get item --vault Verify --account gitlab --fields password "$ONEP_ENTRY")"
ORKA_VPN_CERT="$(op get item --vault Verify --account gitlab --fields "Cert" "$ONEP_ENTRY")"
ORKA_VPN_IP="$(op get item --vault Verify --account gitlab --fields url "$ONEP_ENTRY")"

echo "Connecting to VPN. Enter sudo password..."
echo "$ORKA_VPN_PASS" | sudo openconnect "$ORKA_VPN_IP" --protocol=anyconnect --user="$ORKA_VPN_USER" --servercert "$ORKA_VPN_CERT" --passwd-on-stdin
