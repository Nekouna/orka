FROM debian:buster as build

ARG DUMB_INIT_VERSION=1.2.2

# Download dumb-init
RUN mkdir -p /build/usr/bin && \
    apt-get update -y && \
    apt-get install -yq --no-install-recommends curl ca-certificates unzip && \
    curl -sLJ "https://github.com/Yelp/dumb-init/releases/download/v${DUMB_INIT_VERSION}/dumb-init_${DUMB_INIT_VERSION}_x86_64" -o /build/usr/bin/dumb-init && \
    chmod +x /build/usr/bin/dumb-init

FROM python:3.9

ENV DEBIAN_FRONTEND=noninteractive
ENV PIPENV_PIPFILE=/Pipfile
ENV WORKON_HOME=/venv

COPY --from=build /build /
COPY Pipfile Pipfile.lock Gemfile Gemfile.lock /

# system packages
RUN dumb-init --version && \
    apt-get update -y && \
    apt-get install -yq --no-install-recommends bash openconnect psmisc python3 python3-pip sshpass jq socat rsync && \
    rm -rf /var/lib/apt/lists/*

# use bash as the default shell, mostly to allow for sourcing of rvm's profile
SHELL ["/bin/bash", "-c"]

# python and pip
RUN pip install --upgrade pip pipenv && \
    pipenv install --deploy

# ruby, rvm and bundler
RUN curl -sSL https://get.rvm.io | bash && \
    source /etc/profile.d/rvm.sh && \
    echo "source /etc/profile" >> ~/.bashrc && \
    rvm install 2.7.1 && \
    rvm use 2.7.1 && \
    gem install bundler:2.2.10 && \
    bundle install

ENV PATH="$PATH"

STOPSIGNAL SIGQUIT
ENTRYPOINT ["/usr/bin/dumb-init", "--"]
CMD ["/bin/bash"]
