# frozen_string_literal: true

require 'spec_helper'

context 'roles/package_managers' do
  context 'ruby gems' do
    describe package('bundler') do
      it { should be_installed.by('gem') }

      it 'should install a Gemfile' do
        with_fixtures do |fixture_dir|
          if mojave? || high_sierra? # fix for https://bundler.io/blog/2019/05/14/solutions-for-cant-find-gem-bundler-with-executable-bundle.html
            version = mojave? ? '1.17.1' : '1.16.0'
            cmd = command("sed -ibak 's/2.2.10/#{version}/' #{fixture_dir}/bundler/Gemfile.lock")
            expect(cmd).to be_a_successful_cmd
          end

          cmd = command("cd #{fixture_dir}/bundler && bundle install")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match(/Bundle complete!/)

          cmd = command("cd #{fixture_dir}/bundler && bundle exec rake -T")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match(/Hello, world/)
        end
      end
    end

    describe package('cocoapods') do
      it { should be_installed.by('gem') }

      if high_sierra? || mojave?
        it 'has been setup' do
          expect(file('/Users/gitlab/.cocoapods/repos/master')).to be_directory
        end
      end

      it 'should install a Podfile' do
        with_fixtures do |fixture_dir|
          cmd = command("cp -R #{fixture_dir}/ios/ #{fixture_dir}/cocoapods")
          expect(cmd).to be_a_successful_cmd

          cmd = command("cd #{fixture_dir}/cocoapods && pod install")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match(/Pod installation complete!/)

          file = file("#{fixture_dir}/cocoapods/Podfile.lock")
          expect(file).to be_file
        end
      end
    end

    describe package('fastlane') do
      it { should be_installed.by('gem') }

      it 'should allow us to manipulate an iOS project' do
        with_fixtures do |fixture_dir|
          cmd = command("cp -R #{fixture_dir}/ios/ #{fixture_dir}/fastlane")
          expect(cmd).to be_a_successful_cmd

          cmd = command("cd #{fixture_dir}/fastlane && fastlane ios version")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match(/Version number: 1\.0/)

          file = file("#{fixture_dir}/fastlane/fastlane/report.xml")
          expect(file).to be_file
        end
      end
    end

    describe package('xcpretty') do
      it { should be_installed.by('gem') }

      it 'should format xcodebuild output' do
        with_fixtures do |fixture_dir|
          cmd = command("cat #{fixture_dir}/xcpretty/xcodebuild.log | xcpretty")
          expect(cmd).to be_a_successful_cmd
          expect(cmd.stdout).to match(/Test Succeeded/)
        end
      end
    end
  end
end
